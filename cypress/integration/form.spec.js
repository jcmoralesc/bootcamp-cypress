// Creamos nuestro primer SPEC de Cypress para que se pueda ejecutar

// Creamos nuestro primer grupo (describe) de pruebas (it)
describe("Test de nuestro formulario", () => {

    // Aquí dentro van todas las pruebas que se van a ejecutar
    it('Rellenar el formulario',() => {

        // Navegamos a la raíz de la web
        cy.visit('/');
        // Obtenemos el formulario
        cy.get('form');
        
        cy.get('#name').type("Juancho").should('have.value',"Juancho" )
        cy.get('#email').type("juan.carlos.morales@outlook.com").should('have.value', "juan.carlos.morales@outlook.com")
        cy.get('#message').type("Hola Mundo").should('have.value',"Hola Mundo")

        // Pulsar el botón de submit
        cy.get('form').submit()

    });

    }
);